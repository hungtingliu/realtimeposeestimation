# Real Time Pose Estimation #
* Description:
  Webcam Application to detect in real-time face and show estimated Pitch, Yaw and Roll.
* Author: Vincent Liu
* Date: April 2017
* For more information, please reach the document: [PoseEstimationChallenge.docx](PoseEstimationChallenge.docx)

### Illustrations ###
![Front](illustrations/front.png) 
![Left](illustrations/pitch_negative.png) 
![Right](illustrations/pitch_positive.png) 
![Up](illustrations/yaw_negative.png) 
![Down](illustrations/yaw_positive.png)
![Roll_negative](illustrations/roll_negative.png) 
![Roll_positive](illustrations/roll_positive.png)

### Contact infos ###
>Email: lhungting@gmail.com

>Tel: +886-988028493