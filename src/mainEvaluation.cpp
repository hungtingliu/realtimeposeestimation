/**
  Pose Estimation Challenge:
    Webcam Application to detect in real-time face and show estimated Pitch, Yaw and Roll.
*/

#include <dlib/opencv.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

#include <sstream>
#include <time.h>
#include <fstream>
#include <iostream>

#define IMG_WIDTH 384
#define IMG_HEIGHT 288
#define WIN_NAME "Pose estimation"

using namespace dlib;
using namespace std;
using namespace cv;

/**
 * @brief drawDetectedPoints Draw detected landmark points
 * @param mat
 * @param detectedPoints
 */
void drawDetectedPoints(const Mat& mat, const std::vector<Point2d>& detectedPoints);

/**
 * @brief drawEulerAngles Draw euler angles on the frame
 * @param mat
 * @param eulerAngles
 */
void drawEulerAngles(const Mat& mat, const Vec3d& eulerAngles);

/**
 * @brief drawFrameRate Draw frame rate on the frame
 * @param mat
 * @param frameRate
 */
void drawFrameRate(const Mat& mat, const double& frameRate);

/**
 * @brief calculateEulerAngles Calculate euler angles based on the rotation infos
 * @param rotationMat
 * @return
 */
Vec3d calculateEulerAngles(const Mat& rotationMat);

/**
 * @brief createModelPoints Predefine 3D model points in world coordinates
 * @return
 */
std::vector<Point3d> createModelPoints();

int main()
{
    // 3D world coordinate
    std::vector<Point3d> modelPoints = createModelPoints();

    // 2D image coordinate
    std::vector<Point2d> detectedPoints;

    // Output rotation and translation
    cv::Mat rotation_vector;
    cv::Mat translation_vector;

    // Start and end times
    clock_t startTime, endTime;

    double timeElapsed = 0;

    // Load face detection and pose estimation models.
    frontal_face_detector detector = dlib::get_frontal_face_detector();
    shape_predictor pose_model;
    deserialize("/Users/vincent/Documents/dlib/examples/build/shape_predictor_68_face_landmarks.dat") >> pose_model;

    try
    {

        // Camera internals
        double focal_length = IMG_WIDTH; // Approximate focal length.
        Point2d center = cv::Point2d(IMG_WIDTH/2, IMG_HEIGHT/2);
        cv::Mat camera_matrix = (cv::Mat_<double>(3,3) << focal_length, 0, center.x, 0 , focal_length, center.y, 0, 0, 1);
        cv::Mat dist_coeffs = cv::Mat::zeros(4,1,cv::DataType<double>::type); // Assuming no lens distortion

        namedWindow(WIN_NAME, WINDOW_AUTOSIZE);

        // Apply face detection on database images
        int pan, tilt = 0;
        string panPlus, tiltPlus;
        Mat mat;

        for (int numPers = 1; numPers <= 15; numPers++) {

            // Prepare output file for each category
            fstream resultFs;
            std::stringstream resultFileStream;
            resultFileStream << "Result" << numPers << ".txt";

            string fileName = resultFileStream.str();

            resultFs.open(fileName, ios::out);

            if (!resultFs)
            {
                cout << "Fail to open file:" << fileName << endl;
            }


            for (int numSer = 1; numSer <= 2; numSer++)
            {
                for (int i = 0; i < 93; i++)
                {

                    panPlus  = "";
                    tiltPlus = "";

                    // Retrieve pan and tilt angles
                    if (i == 0)
                    {
                        tilt = -90; pan = 0;
                    }
                    else if (i == 92)
                    {
                        tilt =  90; pan = 0;
                    }
                    else
                    {
                        pan  = ((i - 1) % 13 - 6) * 15;
                        tilt = ((i - 1) / 13 - 3) * 15;
                        if (abs(tilt) == 45) tilt = tilt / abs(tilt) * 60;
                    }

                    // Add "+" before positive angles
                    if (pan >= 0)  panPlus = "+";
                    if (tilt >= 0) tiltPlus = "+";


                    // Load image
                    std::stringstream imgFileStream;
                    string imgFile;

                    imgFileStream << "Person" << setw(2) << setfill('0') << numPers << "/"
                                  << "person" << setw(2) << setfill('0') << numPers << numSer << setw(2) << setfill('0') << i << tiltPlus << tilt << panPlus << pan << ".jpg";

                    imgFileStream >> imgFile;

                    cout << imgFile << endl;

                    mat = imread(imgFile);

                    // Start time
                    startTime = clock();

                    cv_image<bgr_pixel> cimg(mat);

                    // Step 1: Detect faces
                    std::vector<dlib::rectangle> faces = detector(cimg);

                    resultFs << tiltPlus << tilt << " " << panPlus << pan << " ";

                    if (faces.size() == 0)
                    {
                        resultFs << "No faces" << endl;
                    }
                    else
                    {
                        for (unsigned long i = 0; i < faces.size(); ++i)
                        {

                            // Step 2: Find the landmarks of each face.
                            full_object_detection obj = pose_model(cimg, faces[i]);

                            dlib::point noseTip = obj.part(30);
                            dlib::point chin = obj.part(8);
                            dlib::point leftEye = obj.part(45);
                            dlib::point rightEye = obj.part(36);
                            dlib::point leftMouth = obj.part(54);
                            dlib::point rightMouth = obj.part(48);

                            detectedPoints.clear();

                            detectedPoints.push_back(Point2d(noseTip(0), noseTip(1)));
                            detectedPoints.push_back(Point2d(chin(0), chin(1)));

                            detectedPoints.push_back(Point2d(leftEye(0), leftEye(1)));
                            detectedPoints.push_back(Point2d(rightEye(0), rightEye(1)));

                            detectedPoints.push_back(Point2d(leftMouth(0), leftMouth(1)));
                            detectedPoints.push_back(Point2d(rightMouth(0), rightMouth(1)));

                            drawDetectedPoints(mat, detectedPoints);

                            // Step 3: Solve for pose, calculate translation and rotation matrix
                            cv::solvePnP(modelPoints, detectedPoints, camera_matrix, dist_coeffs,
                                         rotation_vector, translation_vector, false, cv::SOLVEPNP_ITERATIVE);

                            // Step 4: Calculate Euler angles
                            Vec3d eulerAngles = calculateEulerAngles(rotation_vector);
                            drawEulerAngles(mat, eulerAngles);

                            resultFs << eulerAngles[0] << " " << eulerAngles[1] << " " << eulerAngles[2] << endl;
                        }

                    }

                    endTime = clock();

                    // Step 5: Calculate frame rate
                    timeElapsed = static_cast<double>(endTime - startTime)/CLOCKS_PER_SEC;

                    drawFrameRate(mat, 1 / timeElapsed);

                    // Step 6: Display the frame
                    imshow(WIN_NAME, mat);

                    // Esc to close the window
                    if (waitKey(33) == 27)
                    {
                        break;
                    }

                }
            }

            resultFs.close();
        }

    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }
}

std::vector<Point3d> createModelPoints()
{
    std::vector<Point3d> modelPoints;

    modelPoints.push_back(Point3d(0.0f, 0.0f, 0.0f));          // Tip of the nose
    modelPoints.push_back(Point3d(0.0f, -330.0f, -65.0f));     // Chin
    modelPoints.push_back(Point3d(-225.0f, 170.0f, -135.0f));  // Left corner of the left eye

    modelPoints.push_back(Point3d(225.0f, 170.0f, -135.0f));   // Right corner of the right eye
    modelPoints.push_back(Point3d(-150.0f, -150.0f, -125.0f)); // Left corner of the mouth
    modelPoints.push_back(Point3d(150.0f, -150.0f, -125.0f));  // Right corner of the mouth

    return modelPoints;
}


void drawDetectedPoints(const Mat& mat, const std::vector<Point2d>& detectedPoints)
{
    for(auto detectedPoint : detectedPoints)
    {
        cv::circle(mat, detectedPoint, 1, Scalar(0,200,0), 2, 8);
    }
}

void drawEulerAngles(const Mat& mat, const Vec3d& eulerAngles)
{
    const double pitch = eulerAngles[0];
    const double yaw = eulerAngles[1];
    const double roll = eulerAngles[2];

    std::stringstream pitchStream, yawStream, rollStream;

    pitchStream << pitch;
    yawStream << yaw;
    rollStream << roll;

    putText(mat,"Pitch: " + pitchStream.str(), Point(10,40), FONT_HERSHEY_PLAIN , 1, Scalar(200,0,0));
    putText(mat,"Yaw: " + yawStream.str(), Point(10,70), FONT_HERSHEY_PLAIN , 1, Scalar(200,0,0));
    putText(mat,"Roll: " + rollStream.str(), Point(10,100), FONT_HERSHEY_PLAIN , 1, Scalar(200,0,0));
}

void drawFrameRate(const Mat& mat, const double& frameRate)
{
    std::stringstream frameRateStream;

    frameRateStream << frameRate;

    putText(mat, "Frame Rate: " + frameRateStream.str(), Point(10,130), FONT_HERSHEY_PLAIN, 1, Scalar(0,0,200));

}

Vec3d calculateEulerAngles(const Mat& rotationMat)
{
    Mat rotMatrix;
    Rodrigues(rotationMat, rotMatrix);

    Vec3d eulerAngles;
    Mat cameraMatrix, transVect, rotMatrixX,rotMatrixY,rotMatrixZ;
    double* _r = rotMatrix.ptr<double>();
    double projMatrix[12] = {_r[0],_r[1],_r[2],0,
                             _r[3],_r[4],_r[5],0,
                             _r[6],_r[7],_r[8],0};

    decomposeProjectionMatrix( Mat(3,4,CV_64FC1,projMatrix),
                               cameraMatrix,
                               rotMatrix,
                               transVect,
                               rotMatrixX,
                               rotMatrixY,
                               rotMatrixZ,
                               eulerAngles);

    return eulerAngles;
}





